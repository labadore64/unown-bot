module.exports = {
	CleanString(str){
        const path = require("path");

		str = str.replace(/_/g, ' ');
		str = str.replace(/-/g, ' ');
        str = path.basename(str);
		return str;
	},
	SendFile(fileID,message){

		const hoot = message.client.commands.get('hoot');
		const { Attachment } = require('discord.js');

        const fs = require("fs");
        const path = require("path");

        stats = fs.statSync(fileID);
        fileSizeInBytes = stats.size;
    
        if((fileSizeInBytes / 1000000.0) > 8){
			hoot.execute(message,"That file is too big to send!");
        }

		if(!fs.existsSync(fileID)){
			hoot.execute(message,"I'm having trouble finding that file right now...");
			return;
		}

		// Send the attachment in the message channel with a content
		hoot.execute(message,"Fetching ``"+path.basename(fileID)+"``...");
		// Create the attachment using MessageAttachment
        const attachment = new Attachment(fileID);
        // Send the attachment in the message channel with a content
        message.channel.send(`${message.author},`, attachment);
	},
	Hoot(message,text){
		const { Client, RichEmbed } = require('discord.js');
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		const embed = new RichEmbed()
		  .setTitle(nickname)
		  .setColor(embed_color)
		  .setThumbnail(img_avatar) 
		  .setDescription(text);
		message.channel.send(embed);
	},
	PokePic(message,text,nickname,embed_color){
		nickname = nickname.toLowerCase();
		
		const { Client, RichEmbed } = require('discord.js');
		var img_avatar = "https://img.pokemondb.net/sprites/diamond-pearl/normal/" + nickname + ".png";
	
			// Do something now you know the image exists.
			const embed = new RichEmbed()
			  .setTitle(nickname)
			  .setColor(embed_color)
			  .setThumbnail(img_avatar) 
			  .setDescription(text);
			message.channel.send(embed);

	},
	UnownPic(message,text,nickname){
		
		const { Client, RichEmbed } = require('discord.js');
		const { embed_color } = require('../preference.json');

		var img_avatar = "https://img.pokemondb.net/sprites/silver/normal/" + nickname + ".png";
	
			// Do something now you know the image exists.
			const embed = new RichEmbed()
			  .setTitle(nickname)
			  .setColor(embed_color)
			  .setThumbnail(img_avatar) 
			  .setDescription(text);
			message.channel.send(embed);

	},
	UnownGenerate(){
		
		var val = Math.floor(
			Math.random() * 26
		  );
		if(val > 25){
				val = 25;
		}
		
		var nickname = "unown-" + String.fromCharCode(65 + val);
		nickname = nickname.toLowerCase();
		
		return nickname;

	},
	makeHex(val){
		if(val > 15){
			return 0;
		}
		
		if(val > 9){
			switch(val){
				case 10:
				return "A";
				
				case 11:
				return "B";
				
				case 12:
				return "C";
				
				case 13:
				return "D";
				
				case 14:
				return "E";
				
				case 15:
				return "F"
			}
		}
		
		return val;
	}
	
};
