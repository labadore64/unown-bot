module.exports = {
	name: 'dex',
	description: 'Print the unown command for a specific unown.',
	execute(message, args) {
		
		// Extract the required classes from the discord.js module
		const { Attachment } = require('discord.js');
		const common = require('../script/common.js');
		
		var letter = "a";
		
		if(args.length > 0){
			letter = args[0].toLowerCase().charAt(0);
		}
		
		var unown_val = letter.toUpperCase().charCodeAt(0)-65;
		
		var fs = require("fs");
		var stringer = fs.readFileSync('./text/unownmode.txt', { 'encoding': 'utf8'}).split("\n");
				
		if(unown_val < 26 && unown_val >= 0){
			var stringe = "**" + stringer[unown_val] + "**";
				
			common.UnownPic(message,stringe,"unown-" + letter);
		
		} else {
			common.Hoot(message,"Unrecognized unown character.");
		}

	}
};
