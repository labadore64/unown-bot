module.exports = {
	name: 'print',
	description: 'Print an image of an unown.',
	execute(message, args) {
		
		// Extract the required classes from the discord.js module
		const { Attachment } = require('discord.js');
		
		var letter = "a";
		
		if(args.length > 0){
			letter = args[0].toLowerCase();
		}
				
		const attachment = new Attachment('./spr/201-'+ letter +'.png');
		// Send the attachment in the message channel with a content
		message.channel.send(attachment);

	}
};
