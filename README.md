# Unown-Bot

![Bot screenshot](/spr/preview.png)

Created with [Discord.js](https://discord.js.org/#/). Please refer to the setup guide to get started.

## Commands

* ``echo`` - Repeats text that you send it.
* ``avatar`` - Displays your pfp.
* ``dex`` - Displays the [Unown Mode](https://bulbapedia.bulbagarden.net/wiki/Unown_Mode) entry for a specific Unown.
* ``print`` - Attaches an image of the requested Unown.
* ``info`` - Bot info.

## File Structure

* ``bot.js`` - The main bot file. This is the actual entry point for the application. It will load your bot token and start running the bot. It will load all the commands found in ``\commands``. It also loads configs from ``config.json``.
* ``runbot.bat`` / ``runbot.sh`` - Shell scripts that run the bot easily for you :) Note that ``runbot.bat`` calls Powershell.
* ``preference.json`` - Preferences for how ``command.Hoot`` displays.
* ``\commands`` - Command files (each command is a separate file). ``bot.js`` will load and scan these commands.
* ``\obj`` - Imported objects built for ease of interface usage with the bot. ``page.js`` adds a function allows for paging between lists (unused in this bot)
* ``\script`` - Includes common scripts used across all bots for easy access. For example, ``Common.Hoot`` which is used to display a textbox.
* ``\spr`` - Sprite files.
* ``\text`` - Text files.
